package com.workout.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "exercises")
@Getter @Setter @NoArgsConstructor
public class ExerciseBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="exercise_id")
    private Integer chestId;
    
    @Column(name="name")
    private String name;
    
    @Column(name="difficulty")
    private Integer difficulty;
    
    @Column(name="reps")
    private String reps;
    
    @Column(name="body_part")
    private String bodyPart;
    
    @Column(name="type")
    private String type;

//    public ExerciseBean() {
//    }

    public ExerciseBean(Integer chestId, String name, Integer difficulty, String reps, String bodyPart, String type) {
        this.chestId = chestId;
        this.name = name;
        this.difficulty = difficulty;
        this.reps = reps;
        this.bodyPart = bodyPart;
        this.type = type;
    }

//    public Integer getChestId() {
//        return chestId;
//    }
//
//    public void setChestId(Integer chestId) {
//        this.chestId = chestId;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Integer getDifficulty() {
//        return difficulty;
//    }
//
//    public void setDifficulty(Integer difficulty) {
//        this.difficulty = difficulty;
//    }
//
//    public String getReps() {
//        return reps;
//    }
//
//    public void setReps(String reps) {
//        this.reps = reps;
//    }
//
//    public String getBodyPart() {
//        return bodyPart;
//    }
//
//    public void setBodyPart(String bodyPart) {
//        this.bodyPart = bodyPart;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }

    @Override
    public String toString() {
        return "ExerciseBean{" + "chestId=" + chestId + ", name=" + name + ", difficulty=" + difficulty + ", reps=" + reps + ", bodyPart=" + bodyPart + ", type=" + type + '}';
    }
    
}
