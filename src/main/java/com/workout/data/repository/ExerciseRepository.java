
package com.workout.data.repository;

import com.workout.data.entity.ExerciseBean;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseRepository extends CrudRepository<ExerciseBean, Integer>{
    
    public Optional<ExerciseBean> findById(Integer id);

    public Iterable<ExerciseBean> findAll();
    
    @Query("SELECT e FROM ExerciseBean e WHERE e.type=:type")
    List<ExerciseBean> findByType(@Param("type") String type);
    
}
