package com.workout.generator;

public enum ExerciseType {
    
    CHEST("Chest"),
    BACK("Back"),
    BICEP("Bicep"),
    TRICEP("Tricep"),
    SHOULDERS("Shoulders"),
    LEGS("Legs");
    
    private final String exerciseName;
    
    private ExerciseType(String exerciseName) {
		this.exerciseName = exerciseName;
	}

	public String getExerciseName() {
		return exerciseName;
	}

	public static ExerciseType findByName(String value) {
		for (ExerciseType type : values()) {
			if (type.exerciseName.equals(value)) {
				return type;
			}
		}
		throw new IllegalArgumentException("Unsupported exercise type: " + value);
	}


}
