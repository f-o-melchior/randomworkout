package com.workout.generator;

import com.workout.data.entity.ExerciseBean;
import java.util.List;
import org.springframework.stereotype.Repository;

public interface RandomWorkoutActivity {

    public List<ExerciseBean> getRandomWorkout(List<ExerciseBean> exercises);
    
}
