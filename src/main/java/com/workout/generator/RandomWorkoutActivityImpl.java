package com.workout.generator;

import com.workout.data.entity.ExerciseBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RandomWorkoutActivityImpl implements RandomWorkoutActivity {

    @Override
    public List<ExerciseBean> getRandomWorkout(List<ExerciseBean> exercises) {
        List<ExerciseBean> result = new ArrayList<>();

        List<List<ExerciseBean>> wrokoutActivity = getWorkoutActivities(exercises);

        wrokoutActivity.stream().flatMap(List::stream).forEach(list -> {
            ExerciseBean bean = new ExerciseBean();
            bean.setBodyPart(list.getBodyPart());
            bean.setName(list.getName());
            bean.setReps(list.getReps());
            bean.setType(list.getType());

            result.add(bean);
        });

        return result;
    }

    private List<List<ExerciseBean>> getWorkoutActivities(List<ExerciseBean> exercises) {

        List<ExerciseBean> chest = orderListsByTypes(exercises, ExerciseType.CHEST);
        List<ExerciseBean> back = orderListsByTypes(exercises, ExerciseType.BACK);
        List<ExerciseBean> bicep = orderListsByTypes(exercises, ExerciseType.BICEP);
        List<ExerciseBean> tricep = orderListsByTypes(exercises, ExerciseType.TRICEP);
        List<ExerciseBean> shoulders = orderListsByTypes(exercises, ExerciseType.SHOULDERS);
        List<ExerciseBean> legs = orderListsByTypes(exercises, ExerciseType.LEGS);

        List<List<ExerciseBean>> result = new ArrayList<List<ExerciseBean>>();

        if (chest.size() != 0) {
            result.add(getRandomWorkoutElements(chest));
        }

        if (tricep.size() != 0) {
            result.add(getRandomWorkoutElements(tricep));
        }

        if (back.size() != 0) {
            result.add(getRandomWorkoutElements(back));
        }

        if (bicep.size() != 0) {
            result.add(getRandomWorkoutElements(bicep));
        }

        if (shoulders.size() != 0) {
            result.add(getRandomWorkoutElements(shoulders));
        }

        if (legs.size() != 0) {
            result.add(getRandomWorkoutElements(legs));
        }

        return result;
    }

    private List<ExerciseBean> orderListsByTypes(List<ExerciseBean> exercises, ExerciseType type) {
        List<ExerciseBean> result = new ArrayList<ExerciseBean>();
        exercises.stream()
                .forEach(
                        exercise -> {
                            if (exercise.getBodyPart().equalsIgnoreCase(type.getExerciseName().toUpperCase())) {
                                result.add(exercise);
                            }
                        }
                );

        return result;
    }

    private List<ExerciseBean> getRandomWorkoutElements(List<ExerciseBean> list) {
        Random rand = new Random();
        List<ExerciseBean> result = new ArrayList<ExerciseBean>();

        int numberOfElements = 4;

        for (int i = 0; i < numberOfElements; i++) {
            int randomIndex = rand.nextInt(list.size());
            ExerciseBean randomElement = new ExerciseBean();
            randomElement = list.get(randomIndex);

            if (randomElement.getBodyPart().equalsIgnoreCase("BICEP") && i == 3
                    || randomElement.getBodyPart().equalsIgnoreCase("TRICEP") && i == 3
                    || randomElement.getBodyPart().equalsIgnoreCase("SHOULDERS") && i == 3) {
                //System.out.println("Skip ->   " + randomElement.toString());
                log.debug("** Skip exercise -> " + randomElement.toString());
            } else {
                result.add(randomElement);
                list.remove(randomIndex);
            }
        }
        return result;
    }
}
