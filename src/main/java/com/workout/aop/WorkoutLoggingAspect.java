package com.workout.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


@Slf4j
@Aspect
@Component
public class WorkoutLoggingAspect {

    @Around("execution(* *..getRandomWorkout(..))")
    public Object randomWorkoutAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        log.debug("**** CALLED - " + proceedingJoinPoint.getSignature());
        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        log.debug("**** FINISHED - " + proceedingJoinPoint.getSignature());
        return value;
    }
    
   @Around("execution(* *..createPlan(..)) && args(type,..)")
    public Object createPlanAroundAdvice(ProceedingJoinPoint proceedingJoinPoint, String type) {     
        
        log.debug("******** START - " + proceedingJoinPoint.getSignature() + " with type - " + type);
        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        log.debug("******** END - " + proceedingJoinPoint.getSignature() + "\n");
        return value;
    }

}