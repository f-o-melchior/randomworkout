package com.workout.web;

import com.workout.data.entity.ExerciseBean;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.workout.data.repository.ExerciseRepository;
import com.workout.generator.RandomWorkoutActivity;

@Controller
public class WorkoutController {

    @Autowired
    ExerciseRepository exerciseRepository;
    
    @Autowired
    RandomWorkoutActivity randomWorkoutService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showIndex(Model model) {

        model.addAttribute("exercise", new ExerciseBean());

        return "index";
    }

    @RequestMapping(value = "/plan", method = RequestMethod.GET)
    public String createPlan(@RequestParam(name = "type", required = true) String type,
            Model model) {

        List<ExerciseBean> exercises = new ArrayList<>();

        if ("MIXED".equals(type)) {
            Iterable<ExerciseBean> results = this.exerciseRepository.findAll();
            results.forEach(exercise -> exercises.add(exercise));

            List<ExerciseBean> workout = randomWorkoutService.getRandomWorkout(exercises);

            model.addAttribute("workout", workout);
        } else {
            List<ExerciseBean> result = this.exerciseRepository.findByType(type);
            result.forEach(exercise -> exercises.add(exercise));

            List<ExerciseBean> workout = randomWorkoutService.getRandomWorkout(exercises);

            model.addAttribute("workout", workout);
        }

        return "workout";
    }

}
