# RandomWorkout

**This project has been created by Bendegúz Gábor-Svanda...**

*This project provides insight into my work...also you can easily generate your own random workout plan. *

---

## Used technologies

1. Spring Framework
2. PostgreSQL
3. Hibernate
4. Thymeleaf
5. Spring AOP
6. CSS


